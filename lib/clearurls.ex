defmodule Clearurls do
  @moduledoc """
  Documentation for `Clearurls`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Clearurls.hello()
      :world

  """
  def hello do
    :world
  end
end
